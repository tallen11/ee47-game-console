#ifndef Accelerometer_h
#define Accelerometer_h

#define Y_MIN 180
#define Y_MAX 512

class Accelerometer
{
public:
	Accelerometer(const int x, const int y, const int z);
	void calibrate();
	void setXRange(const int min, const int max);
	void setYRange(const int min, const int max);
	void setZRange(const int min, const int max);
	int readRawX();	//Reads raw value of X
	int readRawY();	//Reads raw value of X
	int readRawZ();	//Reads raw value of X
	float readX();	//Reads X and converts to value between -1 and 1
	float readY();	//Reads Y and converts to value between -1 and 1
	float readZ();	//Reads Z and converts to value between -1 and 1

private:
	//Pins to read analog data from
	int _xPin;
	int _yPin;
	int _zPin;

	//Values to calibrate
	int _xMax;
	int _xMin;
	int _yMax;
	int _yMin;
	int _zMax;
	int _zMin;
};

#endif