#include "speaker.h"
#include "Arduino.h"

Speaker::Speaker(const int pin) {
	_pin = pin;
}

void Speaker::playTone(const int note) {
	int duration = 1000 / 8;
	tone(_pin, note, duration);
}

void Speaker::playToneSequence(const int* tones, int length) {
	for (int i = 0; i < length; i++) {
		playTone(tones[i]);
		delay(80);
	}

	noTone(_pin);
}
