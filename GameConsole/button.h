/**
 * Button
 * Author: Tate Allen
 *
 * This class cleanly abstracts away button handling.
 */

#ifndef Button_h
#define Button_h

#include "Arduino.h"

#define BUTTON_UP 8
#define BUTTON_DOWN 9
#define BUTTON_A 11
#define BUTTON_B 10

class Button
{
public:
	Button(const unsigned int pin);
	bool isDown();

private:
	unsigned int _pin;
};

#endif
