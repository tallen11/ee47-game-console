#include "Arduino.h"
#include "button.h"

Button::Button(const unsigned int pin) {
	_pin = pin;
	pinMode(_pin, INPUT);
}

bool Button::isDown() {
	return digitalRead(_pin) == 1;
}
