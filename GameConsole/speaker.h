#ifndef Speaker_h
#define Speaker_h

#include "pitches.h"

class Speaker {
public:
	Speaker(const int pin);
	void playTone(const int note);
	void playToneSequence(const int* tones, int length);

private:
	int _pin;
};

#endif
