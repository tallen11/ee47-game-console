#ifndef Point_h
#define Point_h

//Represents a single 2D point or vector
struct Point
{
	float x;
	float y;

	Point(const float xVal, const float yVal) {
		x = xVal;
		y = yVal;
	}
};

#endif