// This code is very procedural and not and all object oriented (with a couple exceptions).
// Writing games in this way for modern hardware is a huge no-no. Abstract your concepts. 
// Due to memory constraints (among other things), the code had to be written like this.

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include "button.h"
#include "accelerometer.h"
#include "speaker.h"

#define CONTRAST 60

#define ROUND_DELAY   1000
#define WINNING_SCORE 4

#define PADDLE_PADDING 	4
#define PADDLE_LENGTH 	8

#define GAMESTATE_PLAYING	0
#define GAMESTATE_PAUSED	1
#define GAMESTATE_NEWROUND	2
#define GAMESTATE_GAMEOVER	3

Adafruit_PCD8544 display(7, 6, 5, -1, -1);
Accelerometer accel(A0, A1, A2);
Speaker speaker(A3);
Button pauseButton(BUTTON_A);

int player1Y;
int player2Y;
float player1Vel;
float player2Vel;
int ballX;
int ballY;
int ballVelX;
int ballVelY;
int player1Score;
int player2Score;
int gameState;

void setup()
{ 
	display.begin();
	display.setContrast(CONTRAST);
  
	player1Y = LCDHEIGHT / 2;
	player2Y = LCDHEIGHT / 2;
	player1Vel = 0.0f;
	player2Vel = 0.0f;

	ballX = 60;
	ballY = 36;
	ballVelX = -1;
	ballVelY = -1;

	accel.setYRange(270, 340);

	randomSeed(analogRead(A5));

	gameState = GAMESTATE_NEWROUND;

	Serial.begin(9600);
}

void loop()
{
	//Render the graphics to the LCD
	render();

	//Update the game "objects"
	update();

	//Constrain the FPS to about 30
	delay(20);
}

void render() {
	display.clearDisplay();

	//Draw the first paddle
	display.drawFastVLine(PADDLE_PADDING, player1Y - PADDLE_LENGTH / 2, PADDLE_LENGTH, BLACK);
	display.drawFastVLine(PADDLE_PADDING + 1, player1Y - PADDLE_LENGTH / 2, PADDLE_LENGTH, BLACK);

	//Draw the second paddle
	display.drawFastVLine(LCDWIDTH - PADDLE_PADDING, player2Y - PADDLE_LENGTH / 2, PADDLE_LENGTH, BLACK);
	display.drawFastVLine(LCDWIDTH - PADDLE_PADDING - 1, player2Y - PADDLE_LENGTH / 2, PADDLE_LENGTH, BLACK);

	if (gameState == GAMESTATE_GAMEOVER) {
		display.setCursor(18, LCDHEIGHT / 2);
		if (player1Score > player2Score) {
			display.print("WINNER!");
		} else {
			display.print("LOSER!");
		}
        display.display();

		delay(2000);

		gameState = GAMESTATE_PLAYING;
		player1Y = LCDHEIGHT / 2;
		player2Y = LCDHEIGHT / 2;
		player1Vel = 0.0f;
		player2Vel = 0.0f;
		ballX = 60;
		ballY = 36;
		ballVelX = -1;
		ballVelY = -1;
		player1Score = 0;
		player2Score = 0;
	} else if (gameState == GAMESTATE_PAUSED) {
		display.setCursor(18, LCDHEIGHT / 2);
		display.print("PAUSED");
		if (pauseButton.isDown()) {
			gameState = GAMESTATE_PLAYING;
		}
	} else {
		//Draw central divider
		display.drawFastVLine(LCDWIDTH / 2, 0, LCDHEIGHT, BLACK);
	}

	//Draw player 1 score
	display.setCursor(LCDWIDTH / 2 - 8, 1);
	display.print(player1Score, DEC);

	//Draw player 2 score
	display.setCursor(LCDWIDTH / 2 + 4, 1);
	display.print(player2Score, DEC);

	//Draw ball
	//display.drawPixel(ballX, ballY, BLACK);
	display.fillRect(ballX - 1, ballY - 1, 3, 3, BLACK);

	display.display();
}

void update() {
	switch (gameState) {
	    case GAMESTATE_PLAYING:
	    	if (pauseButton.isDown()) {
	    		gameState = GAMESTATE_PAUSED;
	    	}

	      	handleBallPhysics();
			handlePaddleCollisions();
			handleScoring();
			aiControl();
			playerControl();
	      	break;

	    case GAMESTATE_NEWROUND:
	      	player1Y = LCDHEIGHT / 2;
	      	player2Y = LCDHEIGHT / 2;
	      	player1Vel = 0.0f;
	      	player2Vel = 0.0f;
	      	ballX = 60;
			ballY = 36;
			ballVelX = -1;
			ballVelY = -1;
	      	
	      	gameState = GAMESTATE_PLAYING;
	      	delay(ROUND_DELAY);
	      	break;

	    case GAMESTATE_PAUSED:
	    case GAMESTATE_GAMEOVER:
	    	break;
	}
}

void handleBallPhysics() {
	//Update ball's position based on it's velocity
	ballX += ballVelX;
	ballY += ballVelY;

	//Keep ball in bounds of screen
	if (ballX - 1 <= 0 || ballX + 1 >= LCDWIDTH - 1) {
		ballVelX = -ballVelX;
	}

	if (ballY - 1 <= 0 || ballY + 1 >= LCDHEIGHT - 1) {
		ballVelY = -ballVelY;
		//speaker.playTone(NOTE_A4);
	}
}

void handlePaddleCollisions() {
	if (ballX - 1 <= PADDLE_PADDING /*&& ballX + 1 >= PADDLE_PADDING*/) {
		if (ballY + 1 >= player1Y - PADDLE_LENGTH / 2 && ballY - 1 <= player1Y + PADDLE_LENGTH / 2) {
			ballVelX = -ballVelX;
			//speaker.playTone(NOTE_G5);
		}
	} else if (ballX + 1 >= LCDWIDTH - PADDLE_PADDING) {
		if (ballY + 1 >= player2Y - PADDLE_LENGTH / 2 && ballY - 1 <= player2Y + PADDLE_LENGTH / 2) {
			ballVelX = -ballVelX;
			//speaker.playTone(NOTE_G5);
		}
	}
}

void handleScoring() {
	if (ballX - 1 <= 0) {
		player2Score++;
		gameState = GAMESTATE_NEWROUND;

		int sadMusic[2] = {NOTE_E5, NOTE_C5};
		speaker.playToneSequence(sadMusic, 2);
	} else if (ballX + 1 >= LCDWIDTH - 1) {
		player1Score++;
		gameState = GAMESTATE_NEWROUND;

		int happyMusic[2] = {NOTE_C5, NOTE_E5};
		speaker.playToneSequence(happyMusic, 2);
	}

	if (player1Score >= WINNING_SCORE || player2Score >= WINNING_SCORE) {
		gameState = GAMESTATE_GAMEOVER;
	}
}

/* Uses a simple PID controller type thing to neatly track the ball. (Only uses the P term) */
void aiControl() {
	int error = 0;
	int r = random(0, 3);
	int r2 = random(2);
	r *= (r2 == 0 ? -1 : 1);

	if (ballVelX > 0) {
		//Track the ball
		error = ballY - (player2Y + r);
		player2Vel = error * 0.2f;
	} else {
		//Move towards center
		error = LCDHEIGHT / 2 - (player2Y + r);
		player2Vel = error * 0.1f;
	}

	player2Y += player2Vel;
	if (player2Y - PADDLE_LENGTH / 2 <= 1) {
		player2Y = PADDLE_LENGTH / 2 + 1;
	} else if (player2Y + PADDLE_LENGTH / 2 >= LCDHEIGHT - 1) {
		player2Y = LCDHEIGHT - PADDLE_LENGTH / 2 - 1;
	}
}

void playerControl() {
	float tilt = -1.0f * accel.readY();
	player1Vel = tilt * 1.2f;
	player1Y += player1Vel;

	if (player1Y - PADDLE_LENGTH / 2 <= 1) {
		player1Y = PADDLE_LENGTH / 2 + 1;
	} else if (player1Y + PADDLE_LENGTH / 2 >= LCDHEIGHT - 1) {
		player1Y = LCDHEIGHT - PADDLE_LENGTH / 2 - 1;
	}
}
