#include "Arduino.h"
#include "accelerometer.h"

Accelerometer::Accelerometer(const int x, const int y, const int z) {
	_xPin = x;
	_yPin = y;
	_zPin = z;

	 _xMax = 0;		//TODO: Record default values for other axes
	 _xMin = 0;
	 _yMax = 512;
	 _yMin = 180;
	 _zMax = 0;
	 _zMin = 0;

	pinMode(_xPin, INPUT);
	pinMode(_yPin, INPUT);
	pinMode(_zPin, INPUT);
}

void Accelerometer::calibrate() {
	int rawX = readRawX();
	int rawY = readRawY();
	int rawZ = readRawZ();

	if (rawX < _xMin) {
		_xMin = rawX;
	}

	if (rawX > _xMax) {
		_xMax = rawX;
	}

	if (rawY < _yMin) {
		_yMin = rawY;
	}

	if (rawY > _yMax) {
		_yMax = rawY;
	}

	if (rawZ < _zMin) {
		_zMin = rawZ;
	}

	if (rawZ > _zMax) {
		_zMax = rawZ;
	}
}

void Accelerometer::setXRange(const int min, const int max) {
	_xMin = min;
	_xMax = max;
}

void Accelerometer::setYRange(const int min, const int max) {
	_yMin = min;
	_yMax = max;
}

void Accelerometer::setZRange(const int min, const int max) {
	_zMin = min;
	_zMax = max;
}

int Accelerometer::readRawX() {
	return analogRead(_xPin);
}

int Accelerometer::readRawY() {
	return analogRead(_yPin);
}

int Accelerometer::readRawZ() {
	return analogRead(_zPin);
}

float Accelerometer::readX() {
	float oldValue = (float)readRawX();
	float oldMin = (float)_xMin;
	float oldMax = (float)_xMax;
	float newMin = -1.0f;
	float newMax = 1.0f;
	
	return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
}

float Accelerometer::readY() {
	float oldValue = (float)readRawY();
	float oldMin = (float)_yMin;
	float oldMax = (float)_yMax;
	float newMin = -1.0f;
	float newMax = 1.0f;
	
	return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
}

float Accelerometer::readZ() {
	float oldValue = (float)readRawZ();
	float oldMin = (float)_zMin;
	float oldMax = (float)_zMax;
	float newMin = -1.0f;
	float newMax = 1.0f;
	
	return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
}